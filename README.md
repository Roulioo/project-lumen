# Lumen PHP Framework

Web application made with lumen.

# Technologies

- LUMEN
- PHPMYADMIN
- GITHUB
- GITLAB

# How to test

1. Run MAMP(mac) or XAMP(windows)
2. Create database "lumen" in phpMyAdmin
3. Go to current folder of project in terminal
4. Run "composer install"
5. Run "php artisan migrate"
6. Run "php artisan --seed"
7. Run "php -S localhost:8000 -t public" to run the server
8. Server will be launched for me is (http://localhost:8000/)
9. Go to your url
10. Enjoy !

# How to test (TODO)

1. Go to current folder of project in terminal
2. Run "docker-compose up"
3. Go to (http://localhost:8080/) and log you
4. Run "docker-compose exec php php artisan migrate"
5. Check you're database
6. Go to (http://localhost:8000/)
7. Enjoy


# Information

We can contact us by email:
- julioocesarr@outlook.fr
- raidali95@yahoo.com
- saidtala23@gmail.com
